#  Contribuir

Como requisito, debes tener un [usuario en GitLab](https://gitlab.com/users/sign_in). El registro es gratuíto.

Este proyecto utiliza [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Se apoya en un [repositorio público de GitLab](https://gitlab.com/coronavirusmakers/info) donde la información se edita en [formato Markdown](https://docs.gitlab.com/ee/user/markdown.html) y mediante un proceso se convierte al [formato HTML que puedes ver en la WEB](https://coronavirusmakers.gitlab.io/info/)

En lugar de hacer los cambios directamente en la WEB, se hará en una copia del proyecto. Para ilustrar el procedimiento vamos a añadir información sobre un tutorial sobre _"Cómo imprimir pantallas 3D protección Covid"_ con un vídeo de Youtube <https://youtu.be/oZ7i5zB9rYA>


1. En todas las páginas encontrarás el icono de un lápiz en la parte superior derecha, debajo de la barra de búsqueda.

    ![Paso 1](img/contributing/tutorial_step1.png "Paso 1")

2. Cuando pinches en el icono de edición te llevará a al repositorio de GitLab, donde puedes editar la página con el botón "Edit"

    ![Paso 2](img/contributing/tutorial_step2.png "Paso 2")

3. GitLab nos indica entonces que para poder editar tienes que hacer una copia del proyecto pulsando sobre el botón "Fork"

    ![Paso  3](img/contributing/tutorial_step3.png "Paso 3")

4. Se creará una copia del proyecto en nuestro espacio personal de GitLab.

    ![Paso  4](img/contributing/tutorial_step4.png "Paso 4")

5. Dispondremos de un editor WEB donde hacer las modificaciones que estimemos oportunas. Como se indicaba al principio, utilizaremos el [formato Markdown](https://docs.gitlab.com/ee/user/markdown.html).

    ![Paso  5](img/contributing/tutorial_step5.png "Paso 5")

6. Si pinchamos sobre el enlace "Preview" podremos ir viendo el aspecto que tendrán los cambios

    ![Paso  6](img/contributing/tutorial_step6.png "Paso 6")

7. Una vez estemos conformes con el resultado, usaremos el botón "Commit changes" para confirmar los cambios realizados. En el cuadro que aparece, indicaremos cuáles son los cambios realizados.

    ![Paso  7](img/contributing/tutorial_step7.png "Paso 7")

8. Se abrirá una página para enviar una petición de cambio, o "Merge Request", desde nuestra copia del proyecto al proyecto original

    ![Paso  8](img/contributing/tutorial_step8.png "Paso 8")

9. Después de enviar la petición de cambio sólo nos queda esperar que alguien del equipo se encarge de revisar e integrar esos cambios

    ![Paso  9](img/contributing/tutorial_step9.png "Paso 9")

10. Los cambios aceptados requieren un proceso posterior de conversión de formato Markdown a formato WEB

    ![Paso 10](img/contributing/tutorial_step10.png "Paso 10")

11. Y este es, por fin, el aspecto final de nuestras modificaciones

    ![Paso 11](img/contributing/tutorial_step11.png "Paso 11")


