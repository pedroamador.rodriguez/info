# Iniciativas I+D

En este documento se recoge información sobre todos los proyectos en marcha de I+D dentro de la comunidad coronavirusmakers

Por lo general, cada projecto usa un canal de Telegram para comunicarse. En la parte superior de estos canales, se encuentra un mensaje anclado. En este mensaje anclado se comparte la informacion más relevante sobre el proyecto, enlaces a los ficheros, etc.

Otros grupos, se organizan a través del [foro](https://foro.coronavirusmakers.org) y van compartiendo allí sus avances.

## Respiradores
### CV19Makers_REESistencia_Team
**Canal de Telegram**: https://t.me/CV19Makers_Ventilador_3D

**Descripción del canal**: De personas para personas

**Descripción del proyecto**: Proyecto para la creación de un respirador abierto utilizando impresión 3D.

**Situación actual**: TBD

### CV19Makers_Ventilador_Fab
**Canal de Telegram**: https://t.me/CV19Makers_Ventilador_Fab

**Descripción del canal**: Inventario de recursos de impresión 3D para emergencias


### CV19Makers_Ventilador_New
**Canal de Telegram**: https://t.me/CV19Makers_Ventilador_New

**Descripción del canal**: -

### Respirador Máscara Decathlon
**Canal de Telegram**: https://t.me/conector3D_mascaraDecathlon

**Descripción del canal**: Impresión 3D para conector máscara Decathlon

### Respirador Máscara Cressi
**Canal de Telegram**: 

**Descripción del canal**:
Pieza fabricada con tecnología FDM que permite usar las mascaras de buceo convencionales con el equipo estandard de un hospital para realizar tratamientos y reducir la sO2 de los pacientes con COVID-19


### Respirador Principe de Asturias
**Canal de Telegram**: https://t.me/respirador_ppe_asturias

**Descripción del canal**: -

### CV19Makers_Pulmon_Simulador
**Canal de Telegram**: https://t.me/joinchat/AHkqjEdA9LgMuVIcbh0kJQ

**Descripción del canal**: Creación de un pulmón artificial o simulador, para poder probar físicamente los diseños de respiradores

### Grupo de trabajo STM32
**Canal de Telegram**:
**Descripción del canal**:


## Material de protección
### Canal de difusión general EPI
**Canal de Telegram**: https://t.me/joinchat/AAAAAEgPpxr4AZxS_BCG8g

**Descripción del canal**: Canal para la publicacion de diseños para trabajar y noticias referente a las necesidades de este EPI

### Viseras
**Canal de Telegram**: https://t.me/joinchat/DdUARlVVQFa7SrHk88udDQ

**Descripción del canal**: -

### Mascarillas UCI
**Canal de Telegram**:

**Descripción del canal**: Mascarillas por inyección de silicona o termoplástico a los que se puedan acoplar filtros HME u otros homologados P2, N95, P3. La mascarilla necesita ser validada/homologada.

### Mascarilla punto wholegarment
**Descripción del canal**: Mascarilla (susceptible de llegar a FFFP2) hecha en punto continuo. 

### Mascarilla lavable textil
**Descripción del canal**: Mascarilla lavable para grupos de riesgo con filtro desechable. 

### Buzos ignífugos
**Descripción del canal**: Desarrollo de patrón abierto y confección libre para desarrollo de un buzo ignífugo e impermeable. 

### Filtros para mascarillas
**Canal de Telegram**: https://t.me/CV19Makers_Mascarillas_Filtros

**Descripción del canal**: El objetivo de este grupo es recopilar y analizar la información en relación a las diferentes tipos de filtros

### CV19Makers_Gafas
**Canal de Telegram**: https://t.me/joinchat/AHkqjFEPwm75gai7IFeMKA

**Descripción del canal**: -

### Mascaras de VMNI
**Descripción del canal**: Mascaras de VMNI basadas en dispositivos tipo de buceo de cobertura completa con adaptación en 3D

### CV19Makers_Helmet
**Canal de Telegram**: https://t.me/joinchat/AHkqjB0JrIvYWVlGhveuew

**Descripción del canal**: -

### EpiCubo
**Descripción del canal**: Cubo de acrílico para evitar salpicaduras durante intubacion [Podemos mejorarlo]

### Tapabocas Sobrasada
**Descripción del canal**: Mascarilla de 3 capas de tela para realizar mediante troquel (incluye ficheros apra troquel)

### CV19Makers_Agarre_Manecilla
**Canal de Telegram**: https://t.me/joinchat/AHkqjEeeH8xooZoAl7c74w

**Descripción del canal**: Este grupo es para búsqueda y diseño de artilugios para agarrar pomos, abrir puertas, tirar del WC, etc.

### Equipo PAPR
**Descripción del canal**: Equipo basado en una capucha textil con un visor fabricado en PETG transparente y sistema de filtro de aire P3. Utilizado en situaciones donde es necesario traje tipo NBQ (agente químicos o biológicos)

### Adaptadores varios para neumología
**Descripción del canal**: Adaptadores varios para nemologia

### Téxtiles
**Canal de Telegram**:

**Descripción del canal**:

## Electrónica
### CV19Makers_Electrónica_Firmware
**Canal de Telegram**: https://t.me/joinchat/AHkqjFVPKkmGlKZW05cH1A

**Descripción del canal**: Para hardware electrónico y firmware de aparatos que sean de ayuda

### qPCR
**Descripción del canal**: Equipo open hardware para realizar copias de ADN o ARN y analizar su cantidad. Util parapoder diagnosticar a la poblacion de forma rapida (2h) y con garantias (tecnica gold standard). Permite descentralizacion y no depender de centros hospitalarios. [POR INICIAR]

## Instrumentación low-cost
### CV19Makers_Capnógrafo
**Canal de Telegram**: https://t.me/joinchat/AHkqjBnI4c5RSNvks5OjxA

**Descripción del canal**: -

### Oxímetro IoT
**Descripción del canal**: Realizar un sistema de monitorización de pacientes de bajo coste, que evite o reduzca al personal sanitario, realizar la medida de constantes de los pacientes como saturación de oxígeno (SpO2) y pulsaciones. Alternativamente también realizar la medición de la temperatura.

## Software
### CV19Makers_Ayuda_Mutua
**Canal de Telegram**: https://t.me/joinchat/AHkqjBtyhNDNZ4Y__b4_cw

**Descripción del canal**: Creación de una aplicación (como software libre) para geolocalizar y conectar necesidades y voluntarios

### App Higiene de manos
**Descripción del canal**: App web para la promoción de la higiene de manos así como la promoción de diferentes métodos de desinfección para la población utilizando medios cercanos y accesibles para la población

### App Gestión de Ayuda
**Descripción del canal**: "Aplicación de gestión que facilita el tratamiento de los productos de ayuda a los puntos de necesidad, con varias modalidades de gestión logística.
Se compone de dos frontales:
A) Frontal híbrido (Web y APP móvil), con dos partes diferenciadas: 
1) Registro de voluntarios, informacion de producción disponible para recogida y requerimientos de material para continuar produciendo.
2) Registro de demanda de productos por parte de entidades.

B) Frontal de gestión para organizar el tratamiento de envíos, con posibilidad de puntos intermedios de almacenamiento y diferentes sistemas de logística (Empresas de ultima milla o fuerzas de seguridad del estado y otras agrupaciones)

Por ultimo, el sistema contiene un backend con datos anonimizados, que permite y facilita la geolocalización de los distintos actores, con el objetivo de poder automatizar las ordenes de trabajo. Una de sus características principales, es la exposición de APIs Rest, que facilita la integración con otros sistemas y canales."

## Generar conocimiento
### Coronavirus 3D Makers Varios
**Canal de Telegram**: https://t.me/covidmakers3dvarios

**Descripción del canal**: Grupo para la creación de ideas previas al desarrollo de un trabajo concreto. ideas antes de crear grupo
## Aportar mi experiencia médica/ingenieríl/etc

### CVMakers Academia
**Descripción del canal**: Desarrollo de contenidos pedagógicos para ayudar a la comunidad a crear dispositivos Open Hardware (impresión 3D, arduino, electrónica, programación, bioinformatica) [EN DESAROLLO] 
TBD
